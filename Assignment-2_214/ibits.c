/*NAME:Musquer Basile
NSID:bam857
Student Number:11287646

University of Saskatchewan
CMPT 214 Term 1 2019
Assignment 2
Part 2
*/

/**************************Pre-Sets****************************/
/*************************************************************/
/* Include */
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <errno.h>

/* Define */
#define BUFFER_SIZE 64
#define INT_SIZE	32

/**************************Functions****************************/
/**************************************************************/
/*----------------------------------------*//* int TooManyArgs(char *) */
int TooManyArgs(char *buff){
	int i=0;

	/* Get rid of ' ' character in front of the input */
	while(i<BUFFER_SIZE && (buff[i]==' ' || buff[i]=='\0')){
		i++;
	}

	/* Check for Index Out of Bound error */
	if(i>=BUFFER_SIZE) return 1;

	/*
	 *	A correct input should have one argument, a number
	 *	We recognize more than one argument if we encounter
	 *	a space in the middle of the input
	*/
	while(i<BUFFER_SIZE){
		if(buff[i]==' '){
			return 1;
		}		
		i++;
	}	
	return 0;
}

/*----------------------------------------*//* int TooFewArgs(char *) */
int TooFewArgs(char *buff){
	int i=0;

	/* Get rid of ' ' character in front of the input */
	while(i<BUFFER_SIZE && (buff[i]==' ' || buff[i]=='\0')){
		i++;
	}
	
	/* Check for Index Out of Bound error */
	if(i>=BUFFER_SIZE) return 1;

	/* 
	 *	Do not consider '+' or '-' character
	 *	Too few arguments is detected if there is not
	 *	at least a character or a digit
	*/
	if(buff[i]=='+' || buff[i]=='-'){
		i++;
	}

	/*
	 *	Thus if what follows a '+' or a '-'
	 *	is a new line, meaning the end of the input,
	 *	then there is too few arguments
	*/
	if(buff[i] == '\n') return 1;
	return 0;
}

/*----------------------------------------*//* int NotAnInteger(char *) */
int NotAnInteger(char *buff){
	int i=0;

	/* Get rid of ' ' character(s) in front of the input */
	while(i<BUFFER_SIZE && (buff[i]==' ' || buff[i]=='\0')){
		i++;
	}
	
	/* Check for Index Out of Bound error */
	if(i>=BUFFER_SIZE) return 1;

	/*
	 *	If the sign is indicate at this point, then continue
	*/
	if(buff[i]=='+' || buff[i]=='-'){
		i++;
	}

	if(i>=BUFFER_SIZE) return 1;


	/*  At this point our index should be on the first digit of the number
	 *	Depending on the format of the number we have three cases:
	 *		Hexadecimal
	 *		Octal
	 *		Decimal
	*/
	switch(buff[i]){
		/* Octal and Hexadecimal cases start with the '0' */		
		case '0':
			i++;
			if(i>=BUFFER_SIZE) return 1;
			switch(buff[i]){
				/* In the hexadecimal case, an 'x' or an 'X' should follow after the zero */
				case 'x':
				case 'X':
					i++;
					if(i>=BUFFER_SIZE) return 1;
					
					/*  
					 *	Keep incrementing our index i as long as the input contains
					 *	based 16 charcaters, to facilitate the comparison, letter from
					 *	'a' to 'f' are set to uppercase
					*/
					while(i<BUFFER_SIZE && ((buff[i]<='9' && buff[i]>='0') || (toupper(buff[i])<='F' && toupper(buff[i])>='A'))){
						i++;
					}
				break;

				/* Octal case is default when preceded by a '0' */
				default:
					/*  
					 *	Keep incrementing our index i as long as the input contains
					 *	based 8 charcaters
					*/
				while(i<BUFFER_SIZE && buff[i]<='7' && buff[i]>='0'){
					i++;
				}
				break;
			}

		break;
		/* Decimal case it the base case when there is digit */
		default:
			// If each digits of teh inputs is a digits of the deca base then its fine
		while(i<BUFFER_SIZE && buff[i]<='9' && buff[i]>='0'){
			i++;
		}
		break;
	}

	if(i>=BUFFER_SIZE) return 1;


	/*
	 * At this point, if something else than the desired character has been met 
	 * then buff[i] is not a new line, but what ever else
	 * Otherwise it means we have reach the end of the input and buff[i]
	 * is the newline character
	*/
	if(buff[i] != '\n'){
		return 1;
	}
	return 0;
}

/*----------------------------------------*//* int IntegerOverflow(char *) */
int IntegerOverflow(char * buff){
	int i=0;
	long int tmp;
	char *eptr;
	errno=0;
	/* Get rid of ' ' character(s) in front of the input */
	while(i<BUFFER_SIZE && (buff[i]==' ' || buff[i]=='\0')){
		i++;
	}
	if(i>=BUFFER_SIZE) return 1;

	/* Do not consider '+' or '-' character to detect an overflow */
	if(buff[i]=='+' || buff[i]=='-'){
		i++;
	}

	if(i>=BUFFER_SIZE) return 1;

	switch(buff[i]){
		/* Octal or Hexadecimal cases */		
		case '0':
			i++;
			if(i>=BUFFER_SIZE) return 1;
			switch(buff[i]){
				/* Hexadecimal */
				case 'x':
				case 'X':
					i++;
					if(i>=BUFFER_SIZE) return 1;
					/*
					 * Check for an overflow thanks to strtol in base 16
					 * If an error is detected then the value of errno, variable from errno.h lib
					 * is set to ERANGE
					*/
					strtol(buff, &eptr, 16);
					if(errno == ERANGE)
						return 1;
				break;

				/* Octal */
				default:
					/*
					 * Check for an overflow thanks to strtol in base 8
					 * If an error is detected then the value of errno, variable from errno.h lib
					 * is set to ERANGE
					*/
					strtol(buff, &eptr, 8);
					if(errno == ERANGE)
						return 1;
				break;
			}

		break;
		/* Decimal */
		default:
			/*
			 * Check for an overflow thanks to strtol in base 8
			 * If an error is detected then the value of errno, variable from errno.h lib
			 * is set to ERANGE
			*/
			strtol(buff, &eptr, 10);
			if(errno == ERANGE)
				return 1;
		break;
	}


	/*
	 * However, strol means string to long (int). Thus at this point we only know if the input
	 * overflows a long int. We store it in a long int
	 * Thus, as we know the boundaries of an int, we just make sure that
	 * the input is within these boundaries
	*/
	sscanf(buff, "%li", &tmp);

	if(tmp>2147483647 || tmp<(-2147483648)){
		return 1;
	}
	
	return 0;
}

/*----------------------------------------*//* void CleanBuffer(char *) */
void  CleanBuffer(char * buff){
	int i;
	/* 
	 *	Fill the buffer with the null character 
	*/
	for(i=0; i<BUFFER_SIZE; i++){
		buff[i]='\0';
	}
}

/**************************MAIN********************************/
/*************************************************************/
int main(void){
	int bits=0;
	char buffer[BUFFER_SIZE];
	int valid=0;
	int i,j;

while(1){
	valid =0;
	CleanBuffer(buffer);
	printf("Choose an integer (64 bits formats), base 8, 10, 16 supported:\n");
	do{
		/*
		 *	Wait for the user to input something.
		 *	Store the input into a buffer
		 *
		 *	Run tests to see if the input is correct
		 *	If not, set valid boolean to 0, meaning
		 *	input is invalid + clean the buffer
		 *
		*/
		if((fgets(buffer, BUFFER_SIZE, stdin)) == NULL ) {return 0;}

		/* Test if input is at least a character or digit long */
		if(TooFewArgs(buffer)){
			printf("Too few arguments. Try again\n");
			valid=0;
			CleanBuffer(buffer);
		}
		/* Test for too many arguments */
		else if(TooManyArgs(buffer)){
			printf("Too many arguments. Try again\n");
			valid=0;
			CleanBuffer(buffer);
		}
		/* Format Test: must be int format in base 10, 8, 16 */
		else if(NotAnInteger(buffer)){
			printf("Not an Integer. Try again\n");
			valid=0;
			CleanBuffer(buffer);
		}
		/* Overflow test: int */
		else if(IntegerOverflow(buffer)){
			printf("Integer overflow. Try again\n");
			valid=0;
			CleanBuffer(buffer);
		}
		/*
		 *	If every tests are succesfull then the input is valid, and we can process on it
		*/
		else valid=1;

		/*
		 *	NOTE: if the input is too big, the program will loop for each set of 63 characters,
		 *			outputing error messages for each invalid set 
		 *			or the answer for the last set if valid.
		*/
	}while(!valid);
	/*
	 *	sscanf does the conversion for us
	 *	from the string input to an int.
	 *	it does consider the base in which
	 *	the int is written
	*/
	sscanf(buffer, " %i", &bits);

	/*
	 *	Shift right bits by i
	 *	We access the i-th bit each iteration
	*/
	for(i=INT_SIZE-1; i>=0;i--){
		j= bits>>i;
		
		/*
		 *	Check the first bit of j
		 *	Knowing i-th bit of bits variable
		*/
		if(j & 1){
			printf("1");
		}
		else
			printf("0");

		/*
		 *	Each 8 bits add a space
		 *	SImulate bytes representation
		*/	
		if(!(i%8)) printf(" ");
	}
	printf("\n");

}
	return 0;
}
