/*NAME:Musquer Basile
NSID:bam857
Student Number:11287646

University of Saskatchewan
CMPT 214 Term 1 2019
Assignment 2
Part 1
*/

/*
 * A buggy program to convert colour images in binary PPM format to greyscale
 * in binary PGM format.  Only "P6" PPM binary format with 
 * intensities < 256 is supported.
 * The first argument on the command line is the input image filename.
 * The second (optional) argument is the output image filename. 
 *  If no output filename is specified, the image is saved in a file with
 *  the same name 
 *  but with a .pgm file extension.  In this case, the input image filename
 *  must end in ".ppm" and can only contain one period, '.'.
 * The final (optional) argument is the gamma value used to convert colour
 *  values to grayscale. The default gamma value is 2.2.
 *
 * The program aborts under any of the following conditions:
 *  the second argument is not provided and the input 
 *  filename is malformed, the input file does
 *  not have a three-line header, the header is malformed, or 
 *  the format of the input file is not "P6" (binary PPM).
 *
 * Program requires that the header consists of the following on three lines:
 *     P6
 *     width height
 *     max_intensity
 * where "width", "height", and "max_intensity" are character representations
 * of numeric values; "width" and "height" are separated by white space;
 * the entire header is terminated by a line terminator.
 * Otherwise, the program assumes that the header is malformed.  The program
 * also assumes that the input image file does not contain any comments.
 * 
 * More information about PPM and PGM format is at
 *   https://en.wikipedia.org/wiki/Netpbm_format
 * or
 *   http://netpbm.sourceforge.net/doc/
 */

/*
 * Standard header files to include
 */
#include <assert.h>  // for assert()
#include <stdbool.h> // for true, false
#include <stdlib.h>  // for NULL, malloc()
#include <string.h>  // for strlen(), strcmp(), strcpy()
#include <math.h>    // for pow()
#include <stdio.h>	 // For file ;)
/*
 * define symbols
 */
#define HDRLINELEN 1024  // maximum length of a line of header information
#define NUMHDRLINES 3    // three header lines
#define BYTESPERPIXEL 3  // 3 bytes (r, g, b) per pixel

/*
 * Global variables
 */
float screen_gamma = 2.2; // default gamma value

/*
 * Auxiliary functions
 */

// create_output_filename_from_input_filename( char* )
//  Replaces the .ppm file extension from the input filename with the 
//  extension .pgm.  The input filename is pointed to by in_filename.
//  On success, returns a pointer to a freshly allocated string (in dynamic
//  memory) with the modified filename.  On failure, causes an abort of
//  the program.  Failure occurs if in_filename does not end in ".ppm"
//  of if there are two or more periods ("dots") in in_filename.
char * create_output_filename_from_input_filename(char * in_filename) {
    char * out_filename;

    // Set up storage for the modified filename
    out_filename = (char *) malloc(strlen(in_filename)); 

    // Copy the input filename up to '.', then give the output filename
    // the ".pgm" extension. Do this by looping through the characters of
    // of in_filename.
    for (unsigned int i=0; i<strlen(in_filename); ++i ) {
        // Once we find a '.' ensure that what follows in the filename is
        // ".ppm"; do not copy the ".ppm", but copy in ".pgm" instead; and
        // return pointer to the new filename. 
        if ( in_filename[i] == '.' ) {
            assert( strcmp( &in_filename[i], ".ppm") == 0 );
            strcpy( &out_filename[i], ".pgm" );
            return out_filename;
        }
        // Haven't found a "." yet, so just copy the next character from
        // input filename to output filename
        out_filename[i] = in_filename[i];
    }

    // if we've reached here then the input filename is invalid.  Abort.
    assert( false );    

    // We should never get to this point, but the last statement of the 
    // function should return a value.
    return NULL;
}

// get_header(FILE*) copies the header (NUMHDRLINES lines of characters)
//  out of the input image 
//  file and stores it in a string in dynamically allocated memory.  
//  The input image is accessible through file stream input_image.  
//  Function returns
//  a pointer to the dynamically allocated string with the header lines.
//  Abort if we cannot read the three-line header.
// The header follows the format:
//  Magic Number (string "P6" for binary colour PPM image)
//  Image width and image height
//  The maximum value per byte (255 for 8-bit images)
char * get_header(FILE * input_image) {
    char header_line[HDRLINELEN];  // space to read into
    char * return_value;
    char * header;                 // pointer to destination memory for header
    char * header_location;

    header = (char *) malloc(NUMHDRLINES*HDRLINELEN+1); // for the stored header
    header_location = header;  // pointer to next location in header 
                               // that is to receive information

    // copy the first NUMHDRLINES lines into the destination string. we fail
    // Abort if to read NUMHDRLINES lines.
    for ( unsigned int i=0; i<NUMHDRLINES; ++i ) {
        return_value=fgets( header_line, sizeof(header_line), input_image );
        assert( return_value != NULL );
        strcpy( header_location, header_line );
        header_location += strlen(header_line);
    }
    // done
    return header;
}

// get_width(char*) retrieves and returns the width (as an integer value)
//  of the image from the header pointed to by header.
//  Abort if the width field cannot be retrieved.
int get_width(char * header) {
    int width;
    assert( sscanf( header, "%*s\n%d%*s", &width ) == 1 );
    return width;
}

// get_height(char*) retrieves and returns the height (as an integer value)
//  of the image from the header pointed to by header.
//  Abort if the height field cannot be retrieved.
int get_height(char * header) {
    int height;
    assert( sscanf( header, "%*s\n%*s%d\n%*s", &height ) == 1 );
    return height;
}

// get_max_value(char*) retrieves and returns the max intensity value (as an
//  integer) of the image from the header pointed to by header.
//  Abort if the field cannot be retrieved.
int get_max_value(char * header) {
    int max_value;
    assert( sscanf( header, "%*s\n%*s\n%d", &max_value ) == 1 );
    return max_value;
}

// get_magic_number(char*) retrieves and returns a pointer to the magic number
//  from the header pointed to by header.  The magic number is stored in a 
//  dynamically allocated array.  Abort if the field cannot be retrieved.
char * get_magic_number(char * header) {
    char * magic_number = malloc(HDRLINELEN);
    assert( sscanf( header, "%s\n%*s", magic_number ) == 1 );
    return magic_number;
}

// create_greyscale_header() creates the header for a greyscale image (.pgm)
//  of the specified width and height.  The header is a dynamically allocated
//  string.  Function returns a pointer to the newly created header.  
//  Function is assumed to always succeed.
char * create_greyscale_header(int width, int height) {
    char * grey_header;
    grey_header = (char*) malloc(HDRLINELEN*NUMHDRLINES + 1);

    sprintf( grey_header, "P5\n%d %d\n255\n", width, height );
    return grey_header;
}


// convert_image(FILE*, FILE*, int, int, int) reads the rgb values from the
//  input image (3 bytes per pixel), converts them to greyscale intensities
//  (1 byte per pixel), and writes them to 
//  the output image.  The input image is read from stream input_file, and
//  the output image is output to stream output_file.  The image has
//  size width by height.  max_value is the maximum intensity value in the
//  input image.
//  Abort if there is an error on input.
void convert_image(FILE * input_file, FILE * output_file, int width, 
                   int height, int max_value) {
    char * row;  // pointer to a row of graphical information

    // dynamically allocated space for a row of pixels.  There are width pixels 
    // described in each row.  Each pixel is described by 3 bytes in an
    // 8-bit PPM image, a byte for red, a byte for green, and a byte for blue.  
    // We also need space for any string/line/row terminator.
    row = malloc( BYTESPERPIXEL*width+1 );  

    // iterate over the height of the image 
    for ( int i=0; i<height; ++i ) {
        // Read in a row of PPM graphical information.  Abort if we
        // cannot read the information.
        assert( (size_t)width == 
                fread( row, BYTESPERPIXEL, width, input_file ) );
        // iterate over the width of a row
        for ( int j=0; j<BYTESPERPIXEL*width; j+=BYTESPERPIXEL ) {
            // Image data is stored as a binary array of rgb values, so for
            // every 3 bytes the first one is the red value, 
            // second is the blue value, and third is the green value
            unsigned char r = row[j];
            unsigned char g = row[j+1];
            unsigned char b = row[j+2];

            // Colour magic. First we convert gamma-corrected rgb values 
            // into their linear values, then we find the luminance in the
            // XYZ colour space, then we convert that to lightness in the 
            // LAB colour space. More info can be found here: 
            // https://en.wikipedia.org/wiki/CIELAB_color_space#CIELAB–CIEXYZ_conversions
            float Y = 0.2126 * pow((float) r/max_value, screen_gamma) + 
                      0.7152 * pow((float) g/max_value, screen_gamma) + 
                      0.0722 * pow((float) b/max_value, screen_gamma);
            float L = 116 * pow(Y, 1.0/3.0) - 16;
            // We now want to drop the decimal places from the floating point 
            // number and convert it to a single byte
            unsigned short int byte = (int) L;   

            // Write out a single greyscale byte to represent the input
            // rgb pixel
            fwrite((char *) &byte, 1, 1, output_file );
        }
    }

}

/*
 * end of Auxiliary functions
 */

// main -  accepts two parameters, argc and argv.  Argv is an array of
//  argc-1 pointers to strings, each of which is an argument on the command
//  line used to invoke this program.  Hence argv[0] is a pointer to the
//  command that was used; argv[1] is a pointer to the required input
//  filename;
//  argv[2], if present, is the name of a file to write the resulting image;
//  argv[3], if present, is the gamma value. 
//  The function returns 0, success, if the input image can be converted 
//  successfully, 1 otherwise.
int main( int argc, char* argv[] ) {
    FILE * in_fp;       // file stream for input image
    FILE * out_fp;      // file stream for output image
    char * out_filename;

    // We need at least two elements in argv, argv[0] and argv[1].  The
    //  latter is the required filename
    if ( argc < 2 ) {
        fprintf( stderr, "Not enough arguments!\n" );
        return 1;
    }

    // Get optional argument for gamma value
    if ( argc >= 4 )
        screen_gamma = strtof( argv[3], NULL );

    // open input file.  Check for error.
    in_fp = fopen( argv[1], "r" );
    if ( in_fp == NULL ) {
        fprintf( stderr, "fopen failed on %s\n", argv[1] );
        return 1;
    }

    // Get optional argument for output file and open the output file
    // Check for error.
    if ( argc >= 3 ){
        out_filename = argv[2];
        out_fp = fopen( out_filename, "w" );
    } else {
        // If no output file is given, create one from the input file
        out_filename = create_output_filename_from_input_filename( argv[1] );
        out_fp = fopen( out_filename, "w" );
    }

    if ( out_fp == NULL ) {
        fprintf( stderr, "fopen failed on %s\n", out_filename );
        return 1;
    }

    // pull the header out of the image and extract all the information 
    // we need from it
    char * header = get_header( in_fp );  
    char * magic_number = get_magic_number( header );   
    assert(strcmp(magic_number, "P6") == 0); // make sure the magic number is "P6"
    int width = get_width( header );
    int height = get_height( header );
    int max_value = get_max_value( header );

    // create a header for the output image
    char * output_header = create_greyscale_header( width, height );
    //write header to output image
    fputs( output_header, out_fp );

    // read, convert the image data, and output the converted data
    convert_image( in_fp, out_fp, width, height, max_value );

    // Done.  Clean up.  Then return with success (0).
    free( header );
    free( magic_number );
    free( output_header );
    fclose( in_fp ); 
    fclose( out_fp );

    return 0;
}

