0a1,10
> /*NAME:Musquer Basile
> NSID:bam857
> Student Number:11287646
> 
> University of Saskatchewan
> CMPT 214 Term 1 2019
> Assignment 2
> Part 1
> */
> 
44c54
< 
---
> #include <stdio.h>	 // For file ;)
81c91
<         if ( in_filename[i] = '.' ) {
---
>         if ( in_filename[i] == '.' ) {
146c156
<     assert( sscanf( header, "%*s\n%*s%d\n%*s", height ) == 1 );
---
>     assert( sscanf( header, "%*s\n%*s%d\n%*s", &height ) == 1 );
255c265
<     if ( argc << 2 ) {
---
>     if ( argc < 2 ) {
273c283
<     if ( argc >= 3 )
---
>     if ( argc >= 3 ){
276c286
<     else {
---
>     } else {
291c301
<     assert( magic_number == "P6" ); // make sure the magic number is "P6"
---
>     assert(strcmp(magic_number, "P6") == 0); // make sure the magic number is "P6"
308c318
<     fclose( *in_fp );
---
>     fclose( in_fp ); 
311c321
<     return;
---
>     return 0;
