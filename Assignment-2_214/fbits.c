/*NAME:Musquer Basile
NSID:bam857
Student Number:11287646

University of Saskatchewan
CMPT 214 Term 1 2019
Assignment 2
Part 3
*/

/************************** Pre-Sets **************************/
/*************************************************************/
/* Include */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

/* Define */
#define BUFFER_SIZE 64
#define FLOAT_SIZE	32
#define SIGN_IND	32
#define EXPO_IND	31
#define MANT_IND	23

/* Union */
union int_float{
	int i;
	float f;
};

/**************************Functions****************************/
/**************************************************************/
/*----------------------------------------*//* int TooManyArgs(char *) */
int TooManyArgs(char *buff){
	int i=0;

	/* Get rid of ' ' character in front of the input */
	while(i<BUFFER_SIZE && (buff[i]==' ' || buff[i]=='\0')){
		i++;
	}

	/* Check for Index Out of Bound error */
	if(i>=BUFFER_SIZE) return 1;

	/*
	 *	A correct input should have one argument, a digit
	 *	We recognize more than one argument if we encounter
	 *	a space in the middle of the input
	*/
	while(i<BUFFER_SIZE){
		if(buff[i]==' '){
			return 1;
		}		
		i++;
	}	
	return 0;
}

/*----------------------------------------*//* int TooFewArgs(char *) */
int TooFewArgs(char *buff){
	int i=0;

	/* Get rid of ' ' character in front of the input */
	while(i<BUFFER_SIZE && (buff[i]==' ' || buff[i]=='\0')){
		i++;
	}
	
	/* Check for Index Out of Bound error */
	if(i>=BUFFER_SIZE) return 1;

	/* 
	 *	Do not consider '+' or '-' character
	 *	Too few arguments is detected if there is not
	 *	at least a character or a digit
	*/
	while(buff[i]=='+' || buff[i]=='-' || buff[i]=='.'){
		i++;
	}

	/*
	 *	Thus if what follows a '+' or a '-' or a '.' character
	 *	is a new line, meaning the end of the input,
	 *	then there is too few arguments
	*/
	if(buff[i] == '\n') return 1;
	return 0;
}

/*----------------------------------------*//* int FloatOverflow(char *) */
int FloatOverflow(char * buff){
	char *eptr;
	errno=0;

	/*
	 *	strtof test if the input is overflowing a float.
	 *	It sets errno to ERANGE if yes
	*/
	strtof(buff, &eptr);
	if(errno == ERANGE)
		return 1;

	return 0;
}

/*----------------------------------------*//* int NotAFloat(char *) */
int NotAFloat(char * buff){
	int i=0;
	int bool_int=0;

	/* Get rid of ' ' character in front of the input */
	while(i<BUFFER_SIZE && (buff[i]==' ' || buff[i]=='\0')){
		i++;
	}

	/* Check for Index Out of Bound error */
	if(i>=BUFFER_SIZE) return 1;


	/*
	 *	At this point, the input is either a character or a digit
	 *	It can be a newline because we have already test for too few arguments
	*/	


	/*
	 *	If the sign is indicate at this point, then continue
	*/
	if(buff[i]=='+' || buff[i]=='-'){
		i++;
	}

	/* Check for Index Out of Bound error */
	if(i>=BUFFER_SIZE) return 1;

	/*
	 *	What follows a '+' or '-' character must either be:
	 *		> Digits for the integer portion
	 *		> A dot if there is no integer portion (no need to write the 0)
	 *			Input such as .32 are accepted
	*/
	while(buff[i]>='0' && buff[i]<='9'){
		bool_int=1;
		i++;
	}

	/* Check for Index Out of Bound error */
	if(i>=BUFFER_SIZE) return 1;

	/*
	 *	At this point, our index has moved by the sign 
	 *	and the integer portion if it exist.
	 *
	 *	If the float has no fractional part and no x10-exponent, then if there is 
	 *	a newline character we can compute.
	 *
	 *	Thanks to the TooFewArgs function we are sure that we are not trying to compute nothing
	*/
	if(buff[i] == '\n') return 0;

	/*
	 *	At this point, either:
	 *		Our index is on a '.', no matter if there is an integer portion, it could be a
	 *		   fractional number
	 *		Our index is on a 'e', if there is previsouly an integer part
	 *		
	*/
	if(buff[i] == '.'){
		i ++;
		if(i>=BUFFER_SIZE) return 1;
		/* A '.' must be followed by digits */
		if(buff[i] == 'e' || buff[i] == 'E') return 1;
	}
	else if(bool_int && (buff[i] == 'e' || buff[i] == 'E')){ }
	else{	return 1;	}

	if(i>=BUFFER_SIZE) return 1;

	/* If there is a newline there it means the input is incomplete */
	if(buff[i] == '\n') return 1;

	/*
	 *	The digits at this point are the fractional part of the number if it exists
	*/
	while(i<BUFFER_SIZE && buff[i]<='9' && buff[i]>='0'){
		i++;
	}
	if(i>=BUFFER_SIZE) return 1;

	/* 
	 *	Now we have deal with the sign, the decimal + fractional part if there are ones.
	 *	We check if an exponent is given
	*/
	switch(buff[i]){
		/* case 1: the floating number is written in exponential notation */
		case 'e':
		case 'E':
			i++;
			if(i>=BUFFER_SIZE) return 1;

			/* Then a '+' or a '-' or a digit must follow */
			if(buff[i] == '+' || buff[i] == '-' || (buff[i]<='9' && buff[i]>='0')){
				/* If it is a sign we move on */
				if(buff[i] == '+' || buff[i] == '-') i++;
			}
			else /* If none of these character it means th eniput is incorrect */
				return 1;

			if(i>=BUFFER_SIZE) return 1;

			/*
			 * If what follows the sign of the exponent is a newline then input is incorrect
			 * It has to be an exponent value if e is indicate
			*/
			if(buff[i] == '\n'){ return 1;}

			/*
			 * If we got this far then it means we should read the exponent's value
			 * Read till something else but a digit is found
			*/
			while(i<BUFFER_SIZE && buff[i]<='9' && buff[i]>='0'){
				i++;
			}

		break;
		/* If the floating number is not in an exponential notation then its fine */
		case '\n':
			return 0;
		default:
		/* Everything else but 'e' or '\n' is not accepted, we also know that it cant be a digit */
			return 1;

	}

	/*
	 * At this point it means that we have reach the end of the floating number, it must have a '\n'
	 * If not the input is rejected
	*/ 
	if(buff[i] != '\n') return 1;

	// Well done input you are correct, cheers !!
	return 0;
}

/*----------------------------------------*//* void CleanBuffer(char *) */
void  CleanBuffer(char * buff){
	int i;
	/* 
	 *	Fill the buffer with the null character 
	*/
	for(i=0; i<BUFFER_SIZE; i++){
		buff[i]='\0';
	}
}

/**************************MAIN********************************/
/*************************************************************/
int main(){
	char buffer[BUFFER_SIZE];
	union int_float bits;
	int valid=0;
	int i, j;

while(1){
	valid=0;
	CleanBuffer(buffer);
	printf("Choose a single-precision IEEE 754 floating-point value:\n");

	do{
		/*
		 *	Wait for the user to input something.
		 *	Store the input into a buffer
         *
		 *	Run tests to see if the input is correct
		 *	If not, set valid boolean to 0, meaning
		 *	input is invalid + clean the buffer
		 *
		*/
		if((fgets(buffer, BUFFER_SIZE, stdin)) == NULL) {return 0;}

		/* Test if input is at least a character or digit long */
		if(TooFewArgs(buffer)){
			printf("Too few arguments. Try again\n");
			valid=0;
			CleanBuffer(buffer);
		}
		/* Test for too many arguments */
		else if(TooManyArgs(buffer)){
			printf("Too many arguments. Try again\n");
			valid=0;
			CleanBuffer(buffer);
		}
		/* Format Test: must be float format */
		else if(NotAFloat(buffer)){
			printf("Input is not a Float. Try again\n");
			valid=0;
			CleanBuffer(buffer);
		}
		/* Overflow test: float */
		else if(FloatOverflow(buffer)){
			printf("Float overflow. Try again\n");
			valid=0;
			CleanBuffer(buffer);
		}
		/*
			If every tests are succesfull then the input is valid, and we can process on it
		*/
		else
			valid=1;

		/*
			NOTE: if the input is too big, the program will loop for each set of 63 characters,
					outputing error messages for each invalid set 
					or the answer for the last set if valid.
		*/

	}while(!valid);

	/*
		sscanf does the conversion for us
		from the string input to an int.

		Indeed we know that the input is a float,
		so it is 32 bits long.

		We are using an union so that we can typed
		this 32 bits pattern as an int even though
		this bits pattern stand for a float
	*/
	sscanf(buffer, "%f", &bits.f);


	/*
		Afterward, just act like this bits pattern is an int
	*/
	for(i=FLOAT_SIZE-1; i>=0;i--){
		j= bits.i>>i;
		
		/*
			Check the first bit of j
			Knowing i-th bit of the bits pattern
		*/
		if(j & 1){
			printf("1");
		}
		else
			printf("0");

		/* 
			Add space in the correct spot
			So that the output looks like the bit representation of a float
		*/
		if(i==SIGN_IND || i==EXPO_IND || i==MANT_IND) printf(" ");
	}
	printf("\n");
}
	return 0;
}
