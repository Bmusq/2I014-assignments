/* Name: Musquer Basile
 * NSID: bam857
 * Student#: 11287646
 * CPMT 214 Fall 2019
 * University of Saskatchewan
 * Assignment 3
*/


/********************************************************************/
                    /*  PRE SETS */
/********************************************************************/
/* INCLUDE */
#include <stdio.h>      /* for printf()         */
#include <stdlib.h>     /* for malloc()         */
#include <limits.h>     /* for INT_MAX          */
#include <stdint.h>     /* for type uint64_t    */
#include <string.h>     /* for strlen()         */
#include <math.h>       /* for floor()          */
#include <ctype.h>      /* for toupper()        */
#include <errno.h>      /* for ERANGE and EINVAL + errno variable*/

/* DEFINE */
#define MIN_SCALE   16
#define MAX_SCALE   64
#define NUM_NTIDES  8

#define BOOL_TRUE   1
#define BOOL_FALSE  0
 
/* STRUCT */
typedef struct coord_t{
    uint64_t n;
    uint64_t q;
}Coord;

typedef struct point_t{
    Coord x;
    Coord y;
}Point;

typedef struct ntide_t{
    Point   pnt;
    char    ltr;    
}Nucle;

/* GLOBAL VARIABLES */
/*
 * Plot: Dynamically allocated, size: Scale*Scale
 */
char **Plot=NULL;

/*
 * Scale: accepted value {16,32,64}
 */
int Scale=0;


/*
 * Corner Points
 */
Point pnt_A;
Point pnt_T;
Point pnt_C;
Point pnt_G;

/********************************************************************/
                    /*  FUNCTIONS */
/********************************************************************/
/* Panic:
 *   Print to stderr an error message
 */
void panic(char *err_msg){
    fprintf(stderr, "[PANIC] %s\n", err_msg);
    exit(1);
}

/* Initialize_plot:
 *  Assign each element of global character array Plot[][] to a space
 *  character, ' '.
 * In:
 *  No input arguments.
 * Out:
 *  All characters in Plot[][] initialized.
 *  No return value.
 * Assumes:
 *   Presence of global variables Plot and Scale.
 *   Plot is of type char** and of dimension Scale X Scale.
 */
void initialize_plot( void ) {
  int i, j;

  for( i=0; i<Scale; i++ ) {
      for( j=0; j<Scale; j++ ) {
          Plot[i][j] = ' ';
      }
  }
}

/* Is_power_of_2:
 *  Given a integer, returns whether it is a power of 2 or not
 *    Return BOOL_TRUE(1)   if val is a power of 2
 *    Return BOOL_FALSE(0)  if val is a power of 2
 * In:
 *  1 args of type int
 * Out:
 *  Value of type _Bool
 * Assumes:
 *  Definition of macros BOOL_TRUE and BOOL_FALSE
 */
_Bool is_power_of_2(int val){
    while(val != 1){
        if(val%2 != 0){
            return BOOL_FALSE;
        }
        val=val/2;
    }
    return BOOL_TRUE;
}

/* Print_plot:
 *  Display on the standart output the content of Plot[][] as well as
 *  a bounding box around the plot and labelled vertices
 * In:
 *  No input arguments.
 * Out:
 *  A representation of the cgr at the time it is called
 *  No return value.
 * Assumes:
 *   Presence of global variables Plot and Scale.
 *   Plot is of type char** and of dimension Scale X Scale.
 */
void print_plot(void){
    int i, j;   
    fprintf(stdout, "C+");
    for(i=0; i<Scale; i++){
        fprintf(stdout, "-");
    }
    fprintf(stdout, "+G\n");

    for(i=Scale-1; i>=0; i--){
        for(j=0; j<Scale; j++){
            if(j==0) fprintf(stdout, " |");
            fprintf(stdout, "%c", Plot[j][i]);
            if(j==Scale-1) fprintf(stdout, "|");
        }
        fprintf(stdout, "\n");
    }

    fprintf(stdout, "A+");
    for(i=0; i<Scale; i++){
        fprintf(stdout, "-");
    }
    fprintf(stdout, "+T\n");
}

/* Scale_coord:
 *  Given a coordinate represented as a rational number, 
 *  returns the integer portion rounded down the nearest integer
 *  of the cooridnate scaled to the global variable Scale
 * In:
 *  1 argument of type Coord {aka struct coord_t}
 *  Coord is composed of 2 values of type uint64_t
 * Out:
 *  Value of type unsigned int
 * Assumes:
 *   Presence of global variables Scale and function floor() {include math.h}
 *   Definition of Coord {aka struct coord_t}
 */
unsigned scale_coord(Coord coord){
    if(coord.q == 0){
        panic("Division by zero: invalid denominator");
    }


    switch(Scale > coord.q){
        case 1: // Scale > d, shift left
            while (coord.q != Scale){
                coord.n = coord.n << 1;
                coord.q = coord.q << 1;
            }    
        case 0: // Scale < d, shift right
            while (coord.q != Scale){
                coord.n = coord.n >> 1;
                coord.q = coord.q >> 1;
            }   
    }
    return coord.n;
}

/* Plot_point:
 *  Given a point, scale both of his coordinates calling scale_coord()
 *  Assuming the result is (col,row)
 *  Assign to the global character array Plot[col][row] the character '*'
 * In:
 *  1 argument of type Point {aka struct point_t}
 *  Point is composed of 2 values of type Coord {aka struct coord_t}
 * Out:
 *  Plot[x_scaled][y_scaled]='*'
 *  No return value
 * Assumes:
 *   Presence of global varaible Plot and function scale_coord().
 *   Definition of Point {aka struct point_t}
 */
void plot_point(Point point){
    Point tmp_point;
    tmp_point.x.n=scale_coord(point.x);
    tmp_point.x.q=1;

    tmp_point.y.n=scale_coord(point.y);
    tmp_point.y.q=1;
    
    Plot[tmp_point.x.n][tmp_point.y.n]='*'; 
}

/* Reduce_coord:
 *  Given a coordinate represented as a rational number,
 *  if the denomiator has his higher-order bits set then
 *  divide both the denominator and the numerator by 2.
 *  Otherwise do nothing
 *  Returns the (un)modified coordinate
 * In:
 *  1 argument of type Coord {aka struct coord_t}
 *  Coord is composed of 2 values of type uint64_t
 * Out:
 *  Value of type Coord {aka struct coord_t}
 * Assumes:
 *  Definition of Coord {aka struct coord_t}
 */
Coord reduce_coord(Coord old_coord){
    if((old_coord.q >> 63) & 1){
        old_coord.q = old_coord.q >> 1;
        old_coord.n = old_coord.n >> 1;
    }
    return old_coord;
}

/* Reduce_point:
 *  Given a point, calls reduce_coord() on both of its coordinates
 *  Returns the (un)modified point
 * In:
 *  1 argument of type Point {aka struct point_t}
 *  Point is composed of 2 values of type Coord {aka struct coord_t}
 * Out:
 *  Value of type Point {aka struct point_t}
 * Assumes:
 *  Definition of Point {aka struct point_t}
 *  Presence of function reduce_coord()
 */
Point reduce_point(Point old_point){
    old_point.x=reduce_coord(old_point.x);
    old_point.y=reduce_coord(old_point.y);
    return old_point;   
}

/* Determine_midpoint:
 *  Given 2 points, determine the mid point between these two points
 *  Returns the computed point
 * In:
 *  2 arguments of type Point {aka struct point_t}
 *  Point is composed of 2 values of type Coord {aka struct coord_t}
 * Out:
 *  Value of type Point {aka struct point_t}
 * Assumes:
 *  Definition of Point {aka struct point_t}
 */
Point determine_midpoint(Point pnt1, Point pnt2){
        /* pnt1 stand for either point {A,T,G,C} */

    Point midpnt;

/* Coordinate X */
    if(pnt1.x.n == 1){
        midpnt.x.n=pnt2.x.n+pnt2.x.q;
        midpnt.x.q=pnt2.x.q << 1;
    }
    else{ // Case: pnt[ATGC].x == 0
        midpnt.x.n=pnt2.x.n;
        midpnt.x.q=pnt2.x.q << 1;   
    }
    
/* Coordinate Y */
    if(pnt1.y.n == 1){
        midpnt.y.n=pnt2.y.n+pnt2.y.q;
        midpnt.y.q=pnt2.y.q << 1;   
    }
    else{ // Case: pnt[ATGC].x == 0
        midpnt.y.n=pnt2.y.n;
        midpnt.y.q=pnt2.y.q << 1;   
    }
    return midpnt;
}

/* Cgr:
 *  Convert the inputed character to upper case
 *  If the character is either {A,T,G,C}:
 *   Calls reduce_point() to reduce the current point (reduction proceed if 
 *   needed)
 *   Determine the corresponding vertex thanks to the inputed character
 *   (switch(in_char))   
 *   Calls determine_midpoint() to calculate the midpoint between the current 
 *   point and the corresponding vertex
 *   Calls plot_point() to set the appropriate element in the character array 
 *   Plot[][] '*'
 *   Returns the computed point as the new current point
 *  If input invalid: 
 *   Returns the current point unchanged
 * In:
 *  1 argument of type Point {aka struct point_t}
 *  1 argument of type char
 * Out:
 *  Value of type Point {aka struct point_t}
 *  Plot[][] appropriately modofied with '*'
 * Assumes:
 *  Presence of function reduce_point(), determine_midpoint(), plot_point(), 
 *  toupper()
 *  Definition of Point {aka struct point_t}
 *  Declaration of the corner point A, T, G, C                  
 */
Point cgr(Point in_pnt, char in_char){
    Point out_pnt;
    in_char=toupper((char) in_char);
    in_pnt=reduce_point(in_pnt);


    switch(in_char){
        case 'A':
            out_pnt=determine_midpoint(pnt_A, in_pnt);
        break;
        case 'C':
            out_pnt=determine_midpoint(pnt_C, in_pnt);
        break;
        case 'T':
            out_pnt=determine_midpoint(pnt_T, in_pnt);
        break;
        case 'G':
            out_pnt=determine_midpoint(pnt_G, in_pnt);
        break;
        default:
            return in_pnt;
    }

    /*  Testing Printf */
    /*  printf("x.n:%lu \t", out_pnt.x.n);
        printf("x.q:%lu \t", out_pnt.x.q);
        printf("y.n:%lu \t", out_pnt.y.n);
        printf("y.q:%lu \n", out_pnt.y.q);  */

    plot_point(out_pnt);
    return out_pnt;
}

/********************************************************************/
                    /*  MAIN */
/********************************************************************/
int main(int argc, char *argv[]){
    int i;
    unsigned j;
    size_t scl;
    char c;
    Nucle curr;
    
    /* Initialize coordinates of corner */
    pnt_A.x.n=0;
    pnt_A.y.n=0;
    pnt_T.x.n=1;
    pnt_T.y.n=0;
    pnt_C.x.n=0;
    pnt_C.y.n=1;
    pnt_G.x.n=1;
    pnt_G.y.n=1;


 /* Verify the number of inputs */
    if (argc != 2){
        panic("usage: ./cgr <N>");
    }

 /* Verify if the input only contains digits */
    for(j=0; j<strlen(argv[1]); j++){
        if(argv[1][j]<'0' || argv[1][j]>'9'){
            panic("Invalid Input: contains non digits character");
        }
    }


    scl = strtol(argv[1], NULL, 10);
 /* Verify if the input contains at least one digit */
    if (errno ==  EINVAL){
        panic("Invalid input: no digits found");
    }
 /* Verify if the number inputed does not overflow the size of a long int */
    if (errno == ERANGE){
        panic("Invalid input: scale out of range");
    }
 /* Verify if the number inputed does not overflow the size of an int */
    if (scl > INT_MAX){
        panic("Invalid Input: integer overflow");
    }
 /* Verify if the number inputed is not negativ or null */
    if (scl <= 0){
        panic("Invalid Input: inferior to 1");
    }

    Scale=(int)scl;
    if (Scale < MIN_SCALE){
        panic("Invalid input: scale too small");
    }
    if (Scale > MAX_SCALE){
        panic("Invalid input: scale too big");
    }
    if (!is_power_of_2(Scale)){
        panic("Invalid Input: scale not a power of 2");
    }

 /* Allocate and Initialize Plot[][] */
    if ((Plot = (char **) malloc(sizeof(char *)*Scale)) == NULL){
        panic("Plot could not be allocated");
    }
    for(i=0; i<Scale; i++){
        if ((Plot[i]=(char *) malloc(sizeof(char)*Scale)) == NULL){
            panic("Plot[i] could not be allocated");
        }
    }
    initialize_plot();

 /* Initialize first point as the point int the middle */
    curr.pnt.x.n=1;
    curr.pnt.x.q=2;
    curr.pnt.y.n=1;
    curr.pnt.y.q=2;

 /* For each character in the standard input until EOF, compute it */
    while((c = (char) getc(stdin)) != EOF){
        curr.ltr = c;
        curr.pnt = cgr(curr.pnt, curr.ltr);
    }

 /* Print the result on the standard output and free the dynamically allocated 
    memory */
    print_plot();
    for(i=0; i<Scale; i++){
        free(Plot[i]);
    }   
    free(Plot);

    return EXIT_SUCCESS;
}
//done



















