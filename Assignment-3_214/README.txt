Musquer Basile
bam857, 11287646

A Makefile is provided for this assignement.


The folder /documentation contains:
    A3_documentation.txt    which is the program manual
    A3_testplan.txt         which contains the guidlines for testing
    A3_testresults.txt      which contains the results for the different tests
                                + the diff between my output and the expected
                                    output from the specifications.

The folder /mine contains:
    mine_DDHIMYHC.txt
    mine_exon18.txt
    mine_HIVPV.txt
    mine_huge.txt
    mine_HUMALBGC.txt
    mine_HUMHBB.txt
    mine_HURASH.txt

These files are the ouput of my programs for the corresponding sequence. You 
can see in the file A3_testresults,txt that I ran it and redirected the output 
to the corresponding mine_file.txt


The folder /plot contains:
    plot_DDHIMYHC.txt
    plot_exon18.txt
    plot_HIVPV.txt
    plot_huge.txt
    plot_HUMALBGC.txt
    plot_HUMHBB.txt
    plot_HURASH.txt

These files contain the expected output given by the specifications for each 
corresponding sequence.
You can see in the file A3_testresults.txt that I ran a diff command between
these plot_files and the mine_files.

You can rerun my program to verify it by yourself. 
You can use the Makefile to compile my program.
Moreover, the genomic sequence are provided in the folder /seq:
    seq_DDHIMYHC.txt
    seq_exon18.txt
    seq_HIVPV.txt
    seq_huge.txt
    seq_HUMALBGC.txt
    seq_HUMHBB.txt
    seq_HURASH.txt

